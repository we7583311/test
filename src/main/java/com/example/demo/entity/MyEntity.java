package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table(value = "t_entity")
@AllArgsConstructor
@NoArgsConstructor
public class MyEntity {
    @Id
    private Long id;
    private String name;
}
