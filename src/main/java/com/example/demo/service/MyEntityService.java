package com.example.demo.service;

import com.example.demo.entity.MyEntity;
import com.example.demo.repository.MyRepository;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class MyEntityService {
    @Resource
    private MyRepository repo;


    public Flux<MyEntity> findAll() {
        return repo.findAll();
    }

    public Mono<MyEntity> generate(MyEntity entity){
        return repo.save(entity);
    }
    public Mono<MyEntity> update(Long id, MyEntity updatedEntity) {
        return repo.findById(id)
                .flatMap(existingEntity -> {
                    existingEntity.setName(updatedEntity.getName());
                    return repo.save(existingEntity);
                });
    }
    public Mono<MyEntity> findById(Long id) {
        return repo.findById(id);
    }
    public Mono<Void> deleteById(Long id) {
        return repo.deleteById(id);
    }

}



