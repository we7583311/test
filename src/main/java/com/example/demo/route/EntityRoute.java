package com.example.demo.route;

import com.example.demo.entity.MyEntity;
import com.example.demo.service.MyEntityService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(EntityRoute.API)
public class EntityRoute {
    public static final String API="/entities";

    @Resource
    private MyEntityService service;
    @GetMapping("/welcome")
    public Mono<String> index(){
        return Mono.just("Hello Violet,welcome to MCD");
    }
    @GetMapping
    public Mono<List<String>> dbTest(){
        return service.findAll().map(MyEntity::getName).collect(Collectors.toList());
    }
    @PostMapping("/save")
    public Mono<MyEntity> save(@RequestBody MyEntity entity){
        return service.generate(entity);
    }
    @PutMapping("/update")
    public Mono<MyEntity> update(@RequestBody MyEntity updatedEntity) {
        Long id = updatedEntity.getId();
        return service.update(id, updatedEntity);
    }
    @GetMapping("/find/{id}")
    public Mono<MyEntity> findById(@PathVariable Long id) {
        return service.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public Mono<Void> deleteById(@PathVariable Long id) {
        return service.deleteById(id);
    }

    }




                                              