package com.example.demo.repository;

import com.example.demo.entity.MyEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface MyRepository extends ReactiveCrudRepository<MyEntity, Long> {

}
